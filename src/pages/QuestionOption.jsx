import React, {useEffect, useState} from "react";
import {Alert, message} from "antd";
import axios from "axios";
import {Server} from "../constants/Constants";


export default function (props) {

    const {questionUuid} = props;

    const [optionList, setOptionList] = useState([]);
    const [selectedOptionUuid, setSelectedOptionUuid] = useState(undefined);
    const [answerResult, setAnswerResult] = useState(undefined);

    useEffect(() => {
        axios
            .get(Server.ENDPOINT_URL + "/option?questionUuid=" + props.questionUuid)
            .then(response => {
                setOptionList(response.data);
            })
    }, [props.questionUuid]);

    const onSelectOption = (option) => {
        setSelectedOptionUuid(option.uuid);
        const requestBody = {
            questionUuid: questionUuid,
            optionUuid: option.uuid
        };
        axios
            .post(Server.ENDPOINT_URL + "/answer", requestBody)
            .then(response => {
                const result = response.data.result;
                setAnswerResult(result);
                if(!result){
                    message.error("Yanlış");
                }else{
                    message.success("Doğru");
                }
            });

    };

    return (
        optionList.length > 0 && (
            optionList.map(option => (
                <div>
                    <Alert
                        message={option.option}
                        type={selectedOptionUuid === option.uuid && answerResult !== undefined ? answerResult ? "success" : "error" : "info" }
                        onClick={() => onSelectOption(option)}
                    />
                </div>
            ))
        )
    );

}