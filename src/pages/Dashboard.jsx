import React, {useEffect, useState} from "react";
import {Button, Card, Steps, message} from "antd";
import axios from "axios";
import {Server} from "../constants/Constants";
import QuestionOption from "./QuestionOption";

const {Step} = Steps;

export default function () {

    const [questionList, setQuestionList] = useState([]);
    const [currentStep, setCurrentStep] = useState(0);


    useEffect(() => {
        axios.get(Server.ENDPOINT_URL + "/question")
            .then(response => {
                setQuestionList(response.data);
            })
    }, []);

    const next = () => {
        setCurrentStep(currentStep + 1);
    };

    const prev = () => {
        setCurrentStep(currentStep - 1);
    };

    return (
        <>
            <Steps size="large" current={currentStep}>
                {questionList.map((question, index) => (
                    <Step/>
                ))}
            </Steps>
            <div className="steps-content">
                {questionList.length > 0 && (
                    <Card title={questionList[currentStep].question}>
                        <QuestionOption
                            questionUuid={questionList[currentStep].uuid}
                        />
                    </Card>
                )}
            </div>
            <div className="steps-action">
                {currentStep > 0 && (
                    <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                        Geri
                    </Button>
                )}
                {currentStep < questionList.length - 1 && (
                    <Button type="primary" onClick={() => next()}>
                        {"İleri"}
                    </Button>
                )}
                {currentStep === questionList.length - 1 && (
                    <Button type="primary" onClick={() => message.success('Tamamlandı.')}>
                        Tamam
                    </Button>
                )}

            </div>
        </>
    );
}